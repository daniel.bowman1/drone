BUILDS=$(patsubst %/Dockerfile,build/%,$(sort $(wildcard */Dockerfile)))
PUSHES=$(patsubst %/Dockerfile,push/%,$(sort $(wildcard */Dockerfile)))

build: pull $(BUILDS)
push: $(PUSHES)

.PHONY: build push

# ---------------------------------------------------------------------

DEPS=$(shell grep -h 'FROM' ./*/Dockerfile | grep -v blacksquaremedia | sed -e 's/FROM //')

pull:
	for dep in $(DEPS); do docker pull $$dep; done

build/%: %
	dep=$$(grep -h 'FROM blacksquaremedia/drone:' $(notdir $@)/Dockerfile | sed -e 's/FROM blacksquaremedia\/drone\:/build\//'); test -n "$$dep" && $(MAKE) $$dep || true
	docker build --force-rm --compress -t blacksquaremedia/drone:$< $</

push/%: % build/%
	docker push blacksquaremedia/drone:$<
